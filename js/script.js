/*eslint-env jquery*/
/*eslint-env common-js*/
/*eslint-env browser*/
/*eslint-env es6*/
/*eslint-env log*/

//Commentaire pour éviter que EsLint nous embete !

function toggleFlex(item) {
    if (item.css("display") == "none")
        item.css("display", "flex");
    else
        item.css("display", "none");
}

function tabSwitch(button, tab) {
    $('.tab').css('display', 'none');
    tab.css('display', 'flex');
    $('.tabButton').removeClass('active');
    button.addClass('active');
}

$(document).ready(function () {
    $('#authButton').click(function () {
        toggleFlex($('#auth'));
    });
    $('.tab').each(function () {
        let button = $('#' + $(this).attr('id') + "_button");
        let tab = $(this);
        button.click(function () {
            tabSwitch(button,
                tab);
        });
    });
    $('.tab.defaultfocus').each(function() {
      tabSwitch($('#' + $(this).attr('id') + "_button"), $(this));
    });
});
