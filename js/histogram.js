var width = 900,
    height = 246,
    barPadding = 1;

window.onload = function(){
  //Récupération des données
  $.getJSON("../php/stats.php", function(data){

    console.log(data);
    var cities = [];
    for(let i=0; i<data.length; i++){
      cities.push(data[i][0]+'-'+data[i][1]);
    }
    console.log(cities);

    //Création d'un élément SVG aux dimensions
    var svg = d3.select(".popular_rides")
                .append("svg")
                .attr("width", width+20)
                .attr("height", height);

   var yRange = d3.scaleLinear().range([height-50, 0]).domain([0, 10]),
       yAxis = d3.axisLeft(yRange);

    //On sélectionne les rectangles et on les ajoute à la zone SVG avec des attributs
    //dépendant des valeurs de data
    svg.selectAll("rect")
       .data(data)
       .enter()
       .append("rect")
       .attr('transform', 'translate(20, -15)')
       .attr("width", width / data.length - barPadding)
       .attr("height", function(d,i) {
         return data[i][2]*20;
       })
       .attr("y", function(d,i) {
         return (height-20) - (data[i][2]*20);
       })
       .attr("x", function(d, i) {
           return i * (width / data.length);
       })
       .attr("fill", function(d,i) {
         return "rgb(0, 0, " + (data[i][2] * 20 + 30) + ")";
       });

      svg.selectAll("text")
         .data(cities)
         .enter()
         .append("text")
         .attr("class", "cities")
         .text(function(d){
           return d;
         })
         .attr("x", function(d, i) {
           return i * (width / data.length)+20;
         })
         .attr("y", 225)
         .attr("font-family", "sans-serif")
         .attr("font-size", "11px")
         .attr("fill", "white");




      svg.selectAll(".info")
         .data(data)
         .enter()
         .append("text")
         .attr('class', 'info')
         .attr('transform', 'translate(15, 0)')
         .text(function(d,i) {
           return data[i][3].toFixed(1)+"€ ~ "+data[i][4]+"min";
         })
         .attr("x", function(d, i) {
           return i * (width / data.length)+7;
         })
         .attr("y", function(d, i) {
           return (height-30) - (data[i][2] * 20) + 5;
         })
         .attr("font-family", "sans-serif")
         .attr("font-size", "11px")
         .attr("fill", "white");

      svg.append("g")
         .attr('transform', 'translate(20, 15)')
         .call(yAxis);
  });
}
