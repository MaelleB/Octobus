<?php
    session_start();
?>
  <!DOCTYPE HTML>
  <html>

  <head>
    <link rel="stylesheet" type="text/css" href="../css/global.css" />
    <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
    <meta charset=utf-8>
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/script.js"></script>
    <script src="../js/histogram.js"></script>
    <title>Notifications</title>
  </head>

  <body>
    <?php
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        include('required.php');
    ?>
      <section id="content">
        <h1>Notifications</h1>
        <section class="articles">
          <?php
          if(isset($_SESSION['login'])) {
            //On supprime les notifications si cela est demandé
            //On supprime tout
            if(isset($_POST['deleteall'])){
              $query = $dbh->prepare(
                'DELETE FROM NOTIF_ADDRESSEE
                WHERE USER_ID = ?'
              );
              $query->execute(array($_SESSION['login']));
            }
            else if(isset($_POST['delete']) && isset($_POST['notif'])){ //On supprime seulement celles sélectionnées.
              $query = $dbh->prepare(
                'DELETE FROM NOTIF_ADDRESSEE
                  WHERE USER_ID = ?
                  AND NOTIF_ID = ?'
                );
              foreach($_POST['notif'] as $notifid) {
                $query->execute(array($_SESSION['login'], $notifid));
              }
            }

            //On affiche les notifications
            $query = $dbh->prepare(
              'SELECT ID, CONTENT
                FROM NOTIFICATION, NOTIF_ADDRESSEE
                WHERE ID = NOTIF_ID
                AND USER_ID = ?');
            $query->execute(array($_SESSION['login']));
            $result = $query->fetchAll();
            if(count($result) == 0) {
              echo surround2('article', surround2('p','Pas de notification pour le moment...'));
            }
            else {
              $form = '<div class="buttonBar">
                <input type="submit" name="delete" id="deletenotif" value="Supprimer la sélection"/>
                <input type="submit" name="deleteall" id="deleteall" value="Supprimer tout"/>
                </div>';
              foreach($result as $row) {
                $notif = '<input type="checkbox" name="notif[]"  value='.$row['ID'].' id="'.$row['ID'].'"/>';
                $notif .= surround('label', 'for="'.$row['ID'].'"',$row['CONTENT']);

                $notif = surround('article', 'class="notif"', $notif);
                $form .= $notif;
              }

              $form = surround('form', 'id="notifform" method="post"', $form);
              echo $form;
            }
          }
          ?>
        </section>
      </section>
      <?php include('footer.php'); ?>
  </body>
