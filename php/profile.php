<?php
    session_start();

    if (isset($_GET['userid'])) {
        $userid = $_GET['userid'];
    } elseif (isset($_SESSION['login'])) {
        $userid = $_SESSION['login'];
    } else {
        header("Location: ./index.php");
    }
?>
  <!DOCTYPE HTML>
  <html>

  <head>
    <link rel="stylesheet" type="text/css" href="../css/global.css" />
    <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
    <meta charset=utf-8>
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/script.js"></script>
    <title>Profil de <?= $userid ?></title>
  </head>

  <body>
    <?php
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        include('required.php');
    ?>
      <section id="content">
        <section class="articles">
          <?php
              if (!isset($_GET['userid']) && isset($_SESSION['login'])) {
                  //Sinon on veut voir le sien.
                  //Si une demande de modification est faite.
                  if (isset($_POST['edit'])) {
                      //On vérifie qu'elle est faite correctement

                      $errorMessages = array();
                      if (!isset($_POST['surname']) || $_POST['surname']=='') {
                          array_push($errorMessages, "Renseignez le nom de famille.");
                      }
                      if (!isset($_POST['name']) || $_POST['name']=='') {
                          array_push($errorMessages, "Renseignez le prénom.");
                      }
                      if (!isset($_POST['phone']) || $_POST['phone']=='') {
                          array_push($errorMessages, "Renseignez le numéro de téléphone.");
                      }

                      //Saisie correcte
                      if (count($errorMessages) == 0) {
                          try {
                              $query = $dbh->prepare(
                        'UPDATE USER
                        SET SURNAME=?, NAME=?, ADDRESS=?, PHONE=?
                        WHERE EMAIL=?'
                      );
                              $query->execute(array($_POST['surname'], $_POST['name'], $_POST['address'], $_POST['phone'], $_SESSION['login']));
                              $_SESSION['name'] = $_POST['name'];
                              $_SESSION['surname'] = $_POST['surname'];
                          } catch (PDOException $e) {
                              echo 'Unable to edit user information (Reason :'.$e->getMessage().')';
                              die();
                          }
                          echo popup(surround2('p', 'Modification de vos informations réussie.'));
                      } else { //Affichage des erreurs
                          echo popup(arrayToUl($errorMessages));
                      }
                  } elseif (isset($_POST['edit_pwd'])) {
                      if (!(isset($_POST['currentpwd']) && isset($_POST['newpwd']) && $_POST['currentpwd']!='' && $_POST['newpwd']!='')) {
                          echo popup(surround2('p', 'Pour modifier votre mot de passe, veuillez entrer votre mot de passe actuel ainsi que le nouveau.'));
                      } else {
                          $query = $dbh->prepare(
                          'SELECT PASSWORD
                          FROM USER
                          WHERE EMAIL=?'
                        );
                          $query->execute(array($_SESSION['login']));
                          $result = $query->fetch();
                          if (md5($_POST['currentpwd']) != $result['PASSWORD']) {
                              echo popup(surround2('p', 'Votre mot de passe actuel ne correspond pas avec ce que vous avez saisi !'));
                          } else {
                              $query = $dbh->prepare(
                              'UPDATE USER SET PASSWORD=? WHERE EMAIL=?'
                            );
                              $query->execute(array(md5($_POST['newpwd']), $_SESSION['login']));
                              echo popup(surround2('p', 'Votre mot de passe a bien été changé !'));
                          }
                      }
                  } elseif (isset($_POST['add_vehicle'])) {
                      if (isset($_POST['make']) && isset($_POST['color']) && $_POST['make'] != '' && $_POST['color'] != '') {
                          $query = $dbh->prepare(
                          'SELECT *
                          FROM VEHICLE
                          WHERE USER_ID = ?
                          AND MAKE = ?
                          AND COLOR = ?'
                        );
                          $query->execute(array($_SESSION['login'], $_POST['make'], $_POST['color']));
                          if (count($query->fetchAll()) > 0) {
                              echo popup(surround2('p', 'Le véhicule existe déjà !'));
                          } else {
                              $query = $dbh->prepare(
                            'INSERT INTO VEHICLE
                            (USER_ID, MAKE, COLOR) VALUES (?, ?, ?)'
                          );
                              $query->execute(array($_SESSION['login'], $_POST['make'], $_POST['color']));
                          }
                      } else {
                          echo popup(surround2('p', 'Veuillez remplir les champs "marque" et "couleur" pour ajouter un véhicule.'));
                      }
                  } elseif (isset($_POST['remove_vehicle']) && isset($_POST['id'])) {
                      $query = $dbh->prepare(
                      'DELETE FROM VEHICLE
                      WHERE USER_ID=?
                      AND ID = ?'
                    );
                      $query->execute(array($_SESSION['login'], $_POST['id']));
                  }

                  $userid = $_SESSION['login'];
                  //Partie permettant la modification des informations.
                  $query = $dbh->prepare(
                  'SELECT SURNAME, NAME, ADDRESS, PHONE
                    FROM USER
                    WHERE EMAIL=?'
                );
                  $query->execute(array($userid));
                  $result = $query->fetch(); ?>
            <article class="center">
              <h2>Modifier votre profil</h2>
              <form method="post" class="edit_profile">
                <label for="surname">Nom de famille</label>
                <input id="surname" name="surname" type="text" value="<?= $result['SURNAME'] ?>" maxlength=20/>

                <label for="name">Prénom</label>
                <input id="name" name="name" type="text" value="<?= $result['NAME'] ?>" maxlength=20/>

                <label for="address">Adresse</label>
                <input id="address" name="address" type="text" value="<?= $result['ADDRESS'] ?>" maxlength=200/>

                <label for="phone">Téléphone</label>
                <input id="phone" name="phone" type="text" pattern="[0-9]{10}" placeholder="0600112233" value="<?= $result['PHONE'] ?>" />

                <input type="submit" name="edit" value="Modifier" />
              </form>
            </article>
            <article class="center">
              <h2>Modifier votre mot de passe</h2>
              <form method="post" class="edit_profile">
                <label for="pwd">Mot de passe actuel</label>
                <input id="pwd" name="currentpwd" type="password" />

                <label for="pwd">Nouveau mot de passe</label>
                <input id="pwd" name="newpwd" type="password" />

                <input type="submit" name="edit_pwd" value="Modifier" />
              </form>
            </article>
            <article class="center">
              <h2>Vos véhicules</h2>
              <h3>Ajout</h3>
              <form method="post" class="edit_profile">
                <label for="make">Marque/Nom</label>
                <input id="make" name="make" type="text" maxlength=20 required pattern="[^;]+"/>

                <label for="color">Couleur</label>
                <input id="color" name="color" type="text" required/>

                <input type="submit" name="add_vehicle" value="Ajouter" />
              </form>
              <hr />
              <h3>Suppression</h3>
              <form method="post" class="edit_profile">
                <select id="make" name="id" >
                <?php
                $query = $dbh->prepare(
                  'SELECT ID, MAKE, COLOR
                  FROM VEHICLE
                  WHERE USER_ID = ?'
                );
                  $query->execute(array($_SESSION['login']));
                  $result = $query->fetchAll();
                  foreach ($result as $row) {
                      echo surround('option', 'value="'.$row[0].'"', $row[1].' '.$row[2]);
                  } ?>
            </select>
                <input type="submit" name="remove_vehicle" value="Supprimer"/>
              </form>


            </article>
            <?php
              }

              if (isset($userid)) {
                  //Affichage du profil
                  $query = $dbh->prepare(
                  'SELECT GENDER, CAP_FIRST(NAME), CAP_FIRST(SURNAME), AGE(EMAIL)
                    FROM USER
                    WHERE EMAIL=?'
                );
                  $query->execute(array($userid));
                  $result = $query->fetch();

                  $name = $result[1];
                  $surname = $result[2];

                  $html =  surround2('h2', $result[1].' '.$result[2]);
                  $list =  surround2('li', (($result[0] == 'M')?'Homme':'Femme').' - '.$result[3].' ans');

                  //Note moyenne de l'utilisateur
                  $query = $dbh->prepare(
                  'SELECT TRUNCATE(AVG(STARS), 1)
                    FROM OPINION
                    WHERE USER_ID=?'
                );
                  $query->execute(array($userid));
                  $result = $query->fetch();
                  if ($result[0] == null) {
                      $list .= surround2('li', 'Pas encore d\'avis.');
                  } else {
                      $list .= surround2('li', surround2('strong', 'Note : ').
                                          $result[0].' <img class="symbol" src="../img/star.png" />');
                  }

                  //Véhicule de l'utilisateur
                  $query = $dbh->prepare(
                    "SELECT CONCAT(MAKE, ' ', COLOR)
                    FROM VEHICLE
                    WHERE USER_ID = ?"
                  );
                  $query->execute(array($userid));
                  $result = $query->fetchAll();
                  $sublist = '';
                  if (count($result) == 0) {
                      $sublist = surround2('li', 'Pas de véhicule renseigné.');
                  } else {
                      $list .= surround2('li', surround2('strong', 'Véhicules :'));
                      foreach ($result as $row) {
                          $sublist .= surround2('li', $row[0]);
                      }
                      $sublist = surround2('ul', $sublist);
                  }
                  $list .= $sublist;

                  //Enfin...
                  $list = surround2('ul', $list);
                  $html = surround2('article', $html.$list);
                  echo $html;
              }

          ?>

        </section>
        <?php if (isset($userid)) {
              echo '<section class="stylish-container">
            <h2>Avis sur '.$name.' '.$surname.'</h2>
            <section class="articles" id="opinions">';
              //On affiche les avis concernant l'utilisateur
              $query = $dbh->prepare(
              'SELECT STARS, COMMENT, NAME, SURNAME, USER_TYPE
              FROM OPINION, USER
              WHERE USER_ID=?
              AND EMAIL=AUTHOR'
            );
              $query->execute(array($userid));
              $result = $query->fetchAll();
              if (count($result) == 0) {
                  echo surround2('p', 'Pas encore d\'avis reçu.');
              }
              foreach ($result as $row) {
                  $article = surround2(
                  'h4',
                $row['STARS']
                .' <img class="symbol" src="../img/star.png" /> de '
                .$row['NAME'].' '
                .$row['SURNAME'].' ('
                .(($row['USER_TYPE']=='PASSENGER')?'Passager':'Conducteur').')'
              );
                  $article .= surround2('p', $row['COMMENT']);
                  $article = surround('article', 'class="opinion"', $article);
                  echo $article;
              }
              echo '</section>';
          }
        ?>
        </section>
      </section>
      <?php include('footer.php'); ?>
  </body>
