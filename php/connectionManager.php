<?php

/* Retours:
 - null si pas connecté à la BDD.
 - true si l'utilisateur est connecté
 - errors (un array) si la saisie utilisateur est mauvaise.
 - false si aucune authentification n'a été demandée.
*/
function sign_in($dbh){

    if($dbh == null){
        return null;
    }


    //On fait les vérifications de saisie
    $errors = array();
    if(isset($_POST['sign_in'])){
        if(!isset($_POST['login'])){
            array_push($errors, "Veuillez renseigner votre email !");
        }
        if(!isset($_POST['password'])){
            array_push($errors, "Veuillez renseigner votre mot de passe !");
        }
    }
    else{
        return false;
    }
    if(count($errors) != 0){
        return $errors;
    }

    //On prépare la requete d'authentification.
    $query = $dbh->prepare('SELECT EMAIL, NAME, SURNAME, ROLE FROM USER WHERE EMAIL=? AND PASSWORD=?');
    $query->execute(array($_POST['login'], md5($_POST['password'])));
    $result = $query->fetch();
    if(!$result){
        array_push($errors, "Aucun utilisateur ne correspond à cette combinaison email/mot de passe.");
        return $errors;
    }
    else{
        $query = $dbh->prepare(
          'SELECT IS_BAN(?)'
        );
        $query->execute(array($_POST['login']));
        if($query->fetch()[0]) { //Si l'utilisateur est banni
          $query = $dbh->prepare(
            'SELECT TYPE, DAY(BAN_END) AS DAY, MONTH(BAN_END) AS MONTH, YEAR(BAN_END) AS YEAR
            FROM USER, BAN
            WHERE BAN_ID = ID
            AND EMAIL = ?'
          );
          $query->execute(array($_POST['login']));
          $result = $query->fetch();
          if($result['TYPE'] == 'PERM'){
            array_push($errors, "Vous ne pouvez vous connecter car vous avez été banni définitivement.");
          }
          else {
            array_push($errors, "Vous ne pouvez pas vous connecter car vous avez été banni jusqu'au ".$result['DAY']."/".$result['MONTH']."/".$result['YEAR'].".");
          }
          return $errors;
        }
        else {
          $_SESSION['login'] = $result['EMAIL'];
          $_SESSION['name'] = $result['NAME'];
          $_SESSION['surname'] = $result['SURNAME'];
          $_SESSION['role'] = $result['ROLE'];
          return true;
        }
    }
}

$return = sign_in($dbh);
if(is_array($return)){
    echo popup(arrayToUl($return));
}
else if($return === true){
    $dbh->exec("SET lc_time_names = 'fr_FR';");
    echo popup(surround('p', '', 'Authentification réussie')
              .surround('p', '', 'Bienvenue '.$_SESSION['name'].' !'));
}

if(isset($_POST['disconnect'])) {
  if(session_destroy()) {
    echo popup(surround('p', '', 'Déconnexion réussie.'));
    unset($_SESSION['login']);
  }
  else {
    echo popup(surround('p', '', 'Désolé, la déconnexion a échouée.'));
  }
}



?>
