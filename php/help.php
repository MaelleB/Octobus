<?php
session_start();
?>

<!DOCTYPE HTML>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="../css/global.css" />
    <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
    <meta charset=utf-8>
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/script.js"></script>
    <title>Accueil</title>
</head>

<body>
    <?php include('required.php');?>
    <section id="content">
        <h1>Aide</h1>
        <section class="articles">
            <article>
              <h2>Le menu</h2>
              <div class="column-article" id="menu_help">
              <img src="../img/menu.png"/>
              <ol>
                <li><strong>Page d'accueil </strong>: Vous trouverez ici les dernières actualités du site.
                </li>
                <li><strong>Trajet</strong> : Vous trouverez ici tout ce qui concerne les trajets (vos trajets conducteur et passager) et la recherche de trajets.
                </li>
                <li>
                  <strong>Notification</strong> : Toutes vos notifications sont affichés dans cette page.
                </li>
                <li>
                  <strong>Profil</strong> : Contient vos informations de profil et vous donne la possibilité de les modifier. Vous pouvez également voir les avis qui vous ont été donnés.
                </li>
                <li>
                  <strong>Connexion</strong> : Bouton de (dé)connexion, il permet aussi de vous inscrire.
                </li>
                <li>
                  <strong>Administration</strong> : Gestion des utilisateurs(bannissement, promotion), statistiques, création/modification de trajet type.
                </li>
                <li>
                  <strong>Aide</strong> : Vous êtes ici !
                </li>
              </ol>
            </div>
            </article>
            <article>
              <h2>À propos</h2>
              <p>Vous voici sur un tout nouveau site de covoiturage réalisé par :
              <ul>
                <li>Beuret Maëlle</li>
                <li>Lesinski Julien</li>
              </ul>
              </p>
            </article>
        </section>
    </section>
    <?php include('footer.php');?>
</body>

</html>
