<?php
session_start();
?>

<!DOCTYPE HTML>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="../css/global.css" />
    <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
    <meta charset=utf-8>
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/script.js"></script>
    <title>Accueil</title>
</head>

<body>
    <?php include('required.php');?>
    <section id="content">
        <h1><img src="../img/logo_white_v2.png" /></h1>
        <section class="articles">
            <article>
              <h2>Bienvenue sur CoRoad</h2>
              <p>
                Bienvenue sur CoRoad, un nouveau site de covoiturage qui on l'espère vous plaira !
              </p>
            </article>
            <article>
              <h2>Fonctionnalités implémentées</h2>
              <ul>
                <li>Compte (inscription, authentification, modification possible des informations du profil)</li>
                <li>Trajet (création, réservation, annulation)</li>
                <li>Avis (rédaction, consultation)</li>
                <li>Administration (gestion des utilisateurs négatifs, statistiques, création de trajet type)</li>
                <li>Notifications (qui permettent au système de tenir au courant les utilisateurs, et aux administrateurs d'envoyer des avertissements)</li>
              </ul>
            </article>
            <article>
                <h2>Une panoplie de logos à choisir !</h2>
                <p>
                    Mais voici de <s>magnifiques</s> logos !
                </p>
                <div class="figure"><img src="../img/logo_white.png" />
                    <legend>Version
                        <emph>white</emph>
                    </legend>
                </div>
                <div class="figure"><img src="../img/logo_white_v2.png" />
                    <legend>Autre version
                        <emph>white</emph>
                    </legend>
                </div>
                <div class="figure"><img src="../img/logo.png" />
                    <legend>Version
                        <emph>black</emph>
                    </legend>
                </div>
            </article>
        </section>
    </section>
    <?php include('footer.php');?>
</body>

</html>
