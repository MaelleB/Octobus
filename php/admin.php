<?php session_start();
ini_set('display_errors', 1);
error_reporting(E_ALL); ?>
<!DOCTYPE HTML>
<html>

<head>
  <link rel="stylesheet" type="text/css" href="../css/global.css" />
  <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
  <meta charset=utf-8>
  <title>Administration</title>
  <script src="../js/d3.min.js"></script>
  <script src="../js/jquery-3.2.1.min.js"></script>
  <script src="../js/script.js"> </script>
  <script src="../js/d3-scale.min.js"></script>
  <script src="../js/histogram.js"></script>
  <title>Administration</title>
</head>

<body>
  <?php include('required.php'); ?>
  <section id="content">
    <?php if(!(isset($_SESSION['role']) && $_SESSION['role'] == 'ADMIN')) {
      echo surround2('h1', 'Vous n\'êtes pas autorisé à voir cette page !');
    }
    else {

    ?>
    <div id="tabs">
      <div class="tabButtons">
        <button class="tabButton active" id="negative_user_button">Gestion des utilisateurs</button>
        <button class="tabButton" id="stat_button">Statistiques</button>
        <button class="tabButton" id="model_ride_button">Modèles de trajet</button>
      </div>
      <div class="tab center
        <?php
        if(!(isset($_POST['modelride']) || isset($_POST['addmodelride']))) {
          echo 'defaultfocus';
        } ?>" id="negative_user">
        <form method="post" id="negative_user_form">
          <?php
            //On traite les demandes
            if(isset($_POST['notify'])) {
              if(isset($_POST['user'])) {
                if(isset($_POST['content'])){
                  $query = $dbh->prepare(
                    'CALL CREATE_NOTIF(?, @ID)'
                  );
                  $query->execute(array($_POST['content']));
                  $query = $dbh->query('SELECT @ID');
                  $notif_id = $query->fetch()[0];
                  foreach($_POST['user'] as $user) {
                    $query = $dbh->prepare(
                      'CALL SEND_NOTIF_TO(?, ?)'
                    );
                    $query->execute(array($user, $notif_id));
                  }
                }
                else {
                  echo popup(surround2('p', 'Veuillez renseigner le corps de la notification !'));
                }
              }
              else {
                echo popup(surround2('p', 'Veuillez sélectionner au moins un utilisateur !'));
              }
            }
            else if(isset($_POST['temporary_ban'])) {
              if(isset($_POST['user'])) {
                if(isset($_POST['day'])){
                  $query = $dbh->prepare(
                    'CALL BAN_USER(?, ?)'
                  );
                  foreach($_POST['user'] as $user) {
                    $query->execute(array($user, $_POST['day']));
                  }
                  $query = $dbh->prepare(
                    'DELETE FROM NEGATIVE_USER
                    WHERE USER_ID = ?'
                  );
                  foreach($_POST['user'] as $user) {
                    $query->execute(array($user));
                  }
                  foreach($_POST['user'] as $user) {
                    //On récupère tous ses Trajets et on l'y désinscrit
                    $query = $dbh->prepare(
                      'SELECT RIDE_ID
                      FROM TRAVELS_IN
                      WHERE USER_ID = ?'
                    );
                    $query->execute(array($user));
                    $result = $query->fetchAll();
                    $query = $dbh->prepare(
                      'CALL CANCELLATION_TRAVELS_IN(?, ?)'
                    );
                    foreach ($result as $row) {
                      $query->execute(array($row[0], $user));
                    }
                  }

                }
                else {
                  echo popup(surround2('p', 'Veuillez renseigner la durée du banissement.'));
                }
              }
              else {
                echo popup(surround2('p', 'Veuillez sélectionner au moins un utilisateur !'));
              }
            }
            else if(isset($_POST['permanent_ban'])) {
              if(isset($_POST['user'])) {
                  $query = $dbh->prepare(
                    'CALL BAN_USER(?, 0)'
                  );
                  foreach($_POST['user'] as $user) {
                    $query->execute(array($user));
                  }
                  $query = $dbh->prepare(
                    'DELETE FROM NEGATIVE_USER
                    WHERE USER_ID = ?'
                  );
                  foreach($_POST['user'] as $user) {
                    $query->execute(array($user));
                  }
                  foreach($_POST['user'] as $user) {
                    //On récupère tous ses Trajets et on l'y désinscrit
                    $query = $dbh->prepare(
                      'SELECT RIDE_ID
                      FROM TRAVELS_IN
                      WHERE USER_ID = ?'
                    );
                    $query->execute(array($user));
                    $result = $query->fetchAll();
                    $query = $dbh->prepare(
                      'CALL CANCELLATION_TRAVELS_IN(?, ?)'
                    );
                    foreach ($result as $row) {
                      $query->execute(array($row[0], $user));
                    }
                  }
              }
              else {
                echo popup(surround2('p', 'Veuillez sélectionner au moins un utilisateur !'));
              }
            }
            else if(isset($_POST['promotion'])) {
              if(isset($_POST['user'])) {
                  $query = $dbh->prepare(
                    "UPDATE USER
                    SET ROLE = 'ADMIN'
                    WHERE EMAIL = ?");
                  $query->execute(array($_POST['user']));
                  if($query->rowCount() == 1) {
                    echo popup(surround2('p', 'Changement effectué !'));
                  }
                  else {
                    echo popup(surround2('p', 'Changement non effectué, veillez à avoir entrer une adresse mail correcte et existante !'));
                  }
              }
            }

            //On affiche les utilisateurs négatifs ainsi que le formulaire
            $query = $dbh->query(
              'SELECT USER_ID, SURNAME, NAME
              FROM NEGATIVE_USER, USER
              WHERE USER_ID=EMAIL');
            if ($query !== false && $query->rowCount() > 0) { ?>
            <div class="actions">
              <div class="stylish-container">
                <textarea name="content" placeholder="Contenu de la notification..." class="long_message_input" maxlength=300></textarea>
                <input type="submit" name="notify" value="Envoyer une notification" />
              </div>
              <div class="stylish-container">
                <div class="rowcenter">
                  <label for="day">Nombre de jour</label>
                  <input type="number" id="day" min=1 name="day" />
                </div>
                <input type="submit" name="temporary_ban" value="Bannir temporairement" />
              </div>
              <div class="stylish-container">
                <input type="submit" name="permanent_ban" value="Bannir définitivement" />
              </div>
            </div>
            <?php
                $html_tab = surround2('tr', surround2('th', 'Email')
                                          .surround2('th', 'Nom de famille')
                                          .surround2('th', 'Prénom')
                                          .surround2('th', 'Sélection'));
                foreach ($query as $row) {
                    $html = surround2("td", surround('a', 'href="profile.php?userid='.$row['USER_ID'].'"', $row['USER_ID']));
                    $html .= surround2("td", $row['SURNAME']);
                    $html .= surround2("td", $row['NAME']);
                    $html .= surround("td", 'class="field_td"', '<input type="checkbox" name="user[]" value="'.$row['USER_ID'].'"/>');


                    $html = surround("tr", "", $html);
                    $html_tab .= $html;
                }
                $html_tab = surround("table", "", $html_tab);
                echo $html_tab;
            }
            else {
              ?>
                <p>Pas d'utilisateur négatif à signaler !</p>
              <?php
            }
          ?>
        </form>
        <div class="stylish-container">
          <form method="post">
            <input type="text" maxlength=254 name="user" placeholder="user@mail.com" required />
            <input type="submit" name="promotion" value="Donner le statut d'administrateur" />
          </form>
        </div>
      </div>
      <div class="tab" id="stat">
        <article class="user_nb">
          <p><strong>Nombre d'utilisateurs : </strong>
          <?php
            $query = $dbh->query("SELECT count(*) FROM USER")->fetch()[0];
            echo $query."</p>";
          ?>
        </article>

        <article class="popular_rides">
          <p><strong>Trajets les plus populaires : </strong></p>
          <?php

          ?>
        </article>
      </div>
      <div class="tab center <?php
      if((isset($_POST['modelride']) || isset($_POST['addmodelride']))) {
        echo 'defaultfocus';
      } ?>" id="model_ride">
        <h2>Modifications</h2>
        <?php
          if(isset($_POST['modelride'])){
            if(!(isset($_POST['hour']) && isset($_POST['min']) && isset($_POST['extra']) && isset($_POST['distance']) && isset($_POST['start']) && isset($_POST['arrival']))) {
              echo popup(surround2('p', 'Désolé une erreur s\'est produite'));
            }
            else {
              $query = $dbh->prepare(
                'UPDATE MODELRIDE
                SET DURATION = ?,
                    DISTANCE = ?,
                    MAX_COST = ?
                WHERE START = ?
                AND ARRIVAL = ?'
              );
              for($i = 0; $i < count($_POST['start']); $i++) {
                if($_POST['start'] != '' && $_POST['arrival'] != ''){
                  if($_POST['hour'][$i]!='' && $_POST['min'][$i]!='') {
                    $duration = $_POST['hour'][$i].':'.$_POST['min'][$i].':'.'00';
                  }
                  else {
                    $duration = NULL;
                  }
                  $distance = ($_POST['distance'][$i]=='')?           NULL:$_POST['distance'][$i];
                  $extra = ($_POST['extra'][$i]=='')?0:$_POST['extra'][$i];
                  $max_cost = ($distance==NULL)?NULL:0.10*$distance+$extra;
                  $query->execute(array($duration, $distance, $max_cost, $_POST['start'][$i], $_POST['arrival'][$i]));
                }
              }
            }
          }
          else if(isset($_POST['addmodelride'])) {
            if(!(isset($_POST['hour']) && isset($_POST['min']) && isset($_POST['extra']) && isset($_POST['distance']) && isset($_POST['start']) && isset($_POST['arrival']))) {
              echo popup(surround2('p', 'Désolé une erreur s\'est produite'));
            }
            else {
              $query = $dbh->prepare(
                'INSERT INTO MODELRIDE (DURATION, DISTANCE, MAX_COST, START, ARRIVAL)
                VALUES (?,?,?,?,?)'
              );
              if($_POST['start'] != '' && $_POST['arrival'] != ''){
                  if($_POST['hour']!='' && $_POST['min']!='') {
                    $duration = $_POST['hour'].':'.$_POST['min'].':'.'00';
                  }
                  else {
                    $duration = NULL;
                  }
                  $distance = ($_POST['distance']=='')?           NULL:$_POST['distance'];
                  $extra = ($_POST['extra']=='')?0:$_POST['extra'];
                  $max_cost = ($distance==NULL)?NULL:0.10*$distance+$extra;
                  try {
                    $query->execute(array($duration, $distance, $max_cost, $_POST['start'], $_POST['arrival']));
                  }
                  catch(PDOException $e) {
                    echo popup(surround2('p', 'Une erreur s\'est produite, vérifiez que le trajet n\'existe pas déjà !'));
                  }
                }
                else {
                  echo popup(surround2('p', 'Ville de départ et d\'arrivée doivent être renseignées.'));
                }
              }
          }

          $html =
            '<tr><th>Départ</th><th>Arrivée</th><th>Durée</th><th>Nombre de Trajets</th><th>Distance</th><th>Dépenses Supplémentaires</th><th>Prix Maximal</th></tr>';
          //On récupère les modèles de trajets en priorisant ceux qui ont des informations incomplètes
          $result = $dbh->query(
            'SELECT *
            FROM MODELRIDE
            WHERE DURATION IS NULL
            OR DISTANCE IS NULL
            OR MAX_COST IS NULL
            ORDER BY FREQUENCY DESC')->fetchAll();
          $result = array_merge($result, $dbh->query(
            'SELECT *
            FROM MODELRIDE
            WHERE DURATION IS NOT NULL
            AND DISTANCE IS NOT NULL
            AND MAX_COST IS NOT NULL
            ORDER BY FREQUENCY DESC')->fetchAll());
          foreach($result as $row) {
            $htmlrow = surround2('td', $dbh->query("SELECT NAME FROM CITY WHERE POST_CODE ='".$row['START']."'")->fetch()[0].' ('.$row['START'].')<input type="hidden" name="start[]" value="'.$row['START'].'" />');
            $htmlrow .= surround2('td', $dbh->query("SELECT NAME FROM CITY WHERE POST_CODE ='".$row['ARRIVAL']."'")->fetch()[0].' ('.$row['ARRIVAL'].')<input type="hidden" name="arrival[]" value="'.$row['ARRIVAL'].'" />');
            if($row['DURATION']){
              $query = $dbh->query("SELECT HOUR('".$row['DURATION']."'), M_FORMAT(MINUTE('".$row['DURATION']."'))");
              $result = $query->fetch();
            }
            else {
              $result = array('','');
            }
            $htmlrow .= surround2('td', '<input type="number" name="hour[]" value="'.$result[0].'" />h<input type="number" name="min[]" value="'.$result[1].'" />');
            $htmlrow .= surround2('td', $row['FREQUENCY']);
            $htmlrow .= surround2('td', '<input type="number" name="distance[]" value="'.$row['DISTANCE'].'" /> km');
            $htmlrow .= surround2('td', '<input type="number" name="extra[]"/> €');
            $htmlrow .= surround2('td', (($row['MAX_COST'])?
            $dbh->query('SELECT TRUNCATE('.$row['MAX_COST'].', 2)')->fetch()[0]
            :
            '-').' €');
            $html.= surround2('tr', $htmlrow);
          }
          $html = surround2('table', $html);
          $html .= '<input type="submit" name="modelride" value="Appliquer les changements" />';
          $html = surround('form', 'method="post"', $html);
          echo $html;
        ?>
        <h2>Création</h2>
        <form method="post">
          <table>
            <tr>
              <th>Départ</th>
              <th>Arrivée</th>
              <th>Durée</th>
              <th>Distance</th>
              <th>Dépenses Supplémentaires</th>
            </tr>
            <tr>
              <td><input type="text" pattern="[0-9]{5}" name="start" placeholder="34090" />
              </td>
              <td><input type="text" pattern="[0-9]{5}" name="arrival" placeholder="13270" />
              </td>
              <td>
                <input type="number" name="hour" value="" placeholder="1"/>h<input type="number" name="min" placeholder="30" />
              </td>
              <td><input type="number" name="distance" placeholder="120" /> km
              </td>
              <td><input type="number" name="extra" placeholder="5"/> €</td>
            </tr>
          </table>
          <input type="submit" name="addmodelride" value="Ajouter" />
        </form>
      </div>
  </section>
  <?php
  }
  include('footer.php'); ?>
</body>
</html>
