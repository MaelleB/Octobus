<?php
  include("connect-database.php");
  $query = $dbh->query("SELECT START_CITY, ARRIVAL_CITY, COUNT(ID), AVG(PRICE), TRUNCATE(AVG(TO_SECONDS(ARRIVAL_DATE)/60 - TO_SECONDS(START_DATE)/60),0) AS AVG_DURATION
                            FROM RIDE
                            GROUP BY START_CITY, ARRIVAL_CITY
                            ORDER BY COUNT(ID) DESC
                            LIMIT 10");
  $array = array();
  while($result = $query->fetch()){
    array_push($array, array((int)$result['START_CITY'], (int)$result['ARRIVAL_CITY'], (int)$result['COUNT(ID)'], (double)$result['AVG(PRICE)'], (double)$result['AVG_DURATION']));
  }

  echo json_encode($array);
?>
