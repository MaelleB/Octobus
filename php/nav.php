<nav>
    <a href="./index.php">
            <img src="../img/home.png" title="Accueil" alt="Accueil"/>
    </a>
    <a href="./rides.php">
            <img src="../img/roadicon.png" title="Trajets" alt="Chercher un trajet"/>
    </a>
    <?php if(isset($_SESSION['login'])) { ?>
    <a href="./notification.php">
      <?php
        $query = $dbh->prepare(
          'SELECT COUNT(*)
          FROM NOTIF_ADDRESSEE
          WHERE USER_ID = ?'
        );
        $query->execute(array($_SESSION['login']));
        $result = $query->fetch()[0];
        if($result != 0){
          if($result > 9) {
            $result = '⁂';
          }
          echo surround('div', 'id="notifnumber"', $result);
        }
      ?>
            <img src="../img/notif.png" title="Notifications"/>
    </a>
    <a href="./profile.php">
        <img src="../img/compte.png" title="Mon profil"/>
    </a>
    <a>
      <form method="post" id="disconnect">
      <input type="submit" name="disconnect" value="" />
    </form>
    </a>
    <?php
  }else{
    ?>
    <a id="authButton">
        <img src="../img/authentification.png" title="S'authentifier"/>
    </a>
    <form method="post" id="auth">
        <input type="text" name="login" placeholder="Email" required>
        <input type="password" name="password" placeholder="Mot de passe" required>
        <div class="submit_div">
            <input type="submit" name="sign_in" value="Se connecter"/>
            <a href="inscription.php"><button type="button" id="sign_up">S'inscrire</button></a>
        </div>
    </form>
    <?php
  }
  if(isset($_SESSION['login']) && $_SESSION['role'] == 'ADMIN') {
  ?>
    <a href="./admin.php">
        <img src="../img/admin.png" title="Administration" alt="Administration"/>
    </a>
  <?php } ?>
    <a href="./help.php">
        <img src="../img/help.png" title="Aide" alt="Aide"/>
    </a>
</nav>
