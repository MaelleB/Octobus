<?php session_start(); ?>
<!DOCTYPE HTML>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="../css/global.css" />
    <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
    <meta charset=utf-8>
    <title>Trajets</title>
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/script.js"></script>
    <title>Trajets</title>
</head>

<body>
    <?php include('required.php'); ?>
    <section id="content">
        <div id="tabs">
            <div class="tabButtons">
                <button class="tabButton" id="my_rides_button">Mes Trajets</button>
                <button class="tabButton" id="search_ride_button">Chercher un trajet</button>
            </div>
            <div class="tab" id="my_rides">
                <article>
                  <?php
                    /*On récupère tous les trajets auxquels l'utilisateur participe sauf ceux où il a déjà donné son avis*/
                    $myrides = $dbh->prepare(
                      "SELECT *
                      FROM RIDE, TRAVELS_IN T1
                      WHERE RIDE_ID=RIDE.ID
                      AND USER_ID=?
                      AND NOT EXISTS
                        (SELECT ID
                        FROM OPINION
                        WHERE RIDE_ID = T1.RIDE_ID
                        AND AUTHOR = T1.USER_ID)");
                    $myrides->execute(array($_SESSION['login']));

                    $rides = $myrides->fetchAll();
                    if($rides != NULL){
                      foreach($rides as $row){
                        $html = surround2('strong', $dbh->query("SELECT NAME FROM CITY WHERE POST_CODE=".$row['START_CITY'])->fetch()[0]);
                        $html.= ' <img src="../img/arrow.png" class="symbol"/> ';
                        $html .= surround2('strong', $dbh->query("SELECT NAME FROM CITY WHERE POST_CODE=".$row['ARRIVAL_CITY'])->fetch()[0]);

                        if($dbh->query("SELECT USER_ID FROM TRAVELS_IN WHERE TYPE='DRIVER' AND RIDE_ID=".$row['ID'])->fetch()[0] == $_SESSION['login']){
                          $html .= " <img src='../img/wheel.png' class='symbol'/>";
                          $html = surround('p', '', $html);
                        }
                        else{
                          $html = surround('p', '', $html);
                          //Récupération du nom et prénom du conducteur
                          $driver = $dbh->query("SELECT NAME, SURNAME FROM TRAVELS_IN, USER WHERE RIDE_ID='".$row['ID']."' AND TYPE='DRIVER' AND USER_ID=EMAIL")->fetch();
                          $html .= surround('p', '', surround('strong', '', 'Conducteur : ').$driver[0]." ".$driver[1]);
                        }
                        //Départ du trajet
                        $time = $dbh->query(
                            "SELECT HOUR(START_DATE) AS HOUR,
                                M_FORMAT(MINUTE(START_DATE)) AS MINUTE,
                                DAY(START_DATE) AS DAY,
                                MONTH(START_DATE) AS MONTH
                                FROM RIDE
                                WHERE ID=".$row['ID'])->fetch();
                        $html .= surround2('p', surround2('strong', 'Départ le : ').$time['DAY'].'/'.$time['MONTH'].' à '.$time['HOUR'].'h'.$time['MINUTE']);
                        //Calcul de la durée estimée du trajet
                        $time = $dbh->query("SELECT TIMEDIFF(ARRIVAL_DATE, START_DATE) FROM RIDE WHERE ID=".$row['ID'])->fetch()[0];
                        //Récupération des heures et des minutes séparément pour les afficher de façon plus cohérente
                        $html .= surround('p', '', surround('strong', '', 'Durée estimée : ').$dbh->query("SELECT HOUR('".$time."')")->fetch()[0]
                                                                                             .'h'.$dbh->query("SELECT M_FORMAT(MINUTE('".$time."'))")->fetch()[0]);
                        $html .= surround('p', '', surround('strong', '', 'Prix du trajet : ').$row["PRICE"].'€');
                        $html .= surround('form', 'method="post" action="ride.php"',
                                                  surround('input', 'name="rideid" type="hidden" value="'.$row["ID"].'"',
                                                                surround('input', 'type="submit" value="Voir le trajet" id="ride"', '')));
                        $html = surround('article', 'class="ride"', $html);
                        echo $html;
                      }
                    }
                    else{
                      echo "<p>Aucun trajet pour le moment...</p>";
                    }
                  ?>
                </article>
            </div>
            <div class="tab defaultfocus" id="search_ride">

                    <fieldset>
                        <legend>Rechercher un trajet</legend>
                        <form method="post">
                        <input required type="text" placeholder="Ville de départ" name="start_city" />
                        <input required type="text" placeholder="Ville d'arrivée" name="arrival_city" />
                        <input name="date" type="date" min="<?= date('Y-m-d'); ?>" placeholder="<?= date('d/m/Y'); ?>" pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}" />
                        <div class="labelfield"><label for="people">Nombre de places</label>
                            <input required type="number" min=1 value=1 name="people" />
                        </div>
                        <input type="submit" name="search_ride" value="Rechercher" />
                        </form>
                        <?php if(isset($_SESSION['login'])) { ?>
                          <form method="post" action="new-ride.php">
                            <input type="submit" name="newride" value="Proposer un trajet"/>
                          </form>
                        <?php } ?>
                    </fieldset>

                <?php
                  if(isset($_POST['start_city']) && isset($_POST['arrival_city'])
                                                && isset($_POST['date'])
                                                && isset($_POST['people']) ){
                    //On prépare la requête récupérant tous les trajets correspondant à la recherche
                    $query = $dbh->prepare("SELECT * FROM RIDE
                                            WHERE START_CITY=
                                              (SELECT POST_CODE FROM CITY WHERE NAME=?)
                                              AND ARRIVAL_CITY=(SELECT POST_CODE FROM CITY WHERE NAME=?)
                                              AND SEAT_NB >=?
                                              AND DATE(START_DATE)=?");

                    //On exécute la requête avec les résultats de la recherche
                    $query->execute(array($_POST["start_city"], $_POST["arrival_city"], $_POST["people"], $_POST["date"]));
                    $result = $query->fetchAll();

                    //Pour chaque résultat, on affiche les informations du trajet
                    foreach($result as $row){
                      $html = surround('strong', '', $_POST["start_city"]);
                      $html .= ' <img src="../img/arrow.png" class="symbol"/> ';
                      $html .= surround('strong', '', $_POST["arrival_city"]);
                      $html = surround('p', '', $html);
                      //Récupération du nom et prénom du conducteur
                      $driver = $dbh->query("SELECT NAME, SURNAME FROM TRAVELS_IN, USER WHERE RIDE_ID='".$row['ID']."' AND TYPE='DRIVER' AND USER_ID=EMAIL")->fetch();
                      $html .= surround('p', '', surround('strong', '', 'Conducteur : ').$driver[0]." ".$driver[1]);
                      //Calcul de la durée estimée du trajet
                      $time = $dbh->query("SELECT TIMEDIFF(ARRIVAL_DATE, START_DATE) FROM RIDE WHERE ID=".$row['ID'])->fetch()[0];
                      //Récupération des heures et des minutes séparément pour les afficher de façon plus cohérente
                      $html .= surround('p', '', surround('strong', '', 'Durée estimée : ').$dbh->query("SELECT HOUR('".$time."')")->fetch()[0]
                                                                                           .'h'.$dbh->query("SELECT MINUTE('".$time."')")->fetch()[0]);
                      $html .= surround('p', '', surround('strong', '', 'Prix du trajet : ').$row["PRICE"].'€');
                      $html .= surround('form', 'method="post" action="ride.php"',
                                                surround('input', 'name="rideid" type="hidden" value="'.$row["ID"].'"',
                                                              surround('input', 'type="submit" value="Voir le trajet" id="ride'.$row["ID"].'"', '')));
                      $html = surround('article', 'class="ride"', $html);
                      echo $html;
                    }

                  }
                ?>

            </div>
        </div>
    </section>
    <?php include('footer.php'); ?>
</body>

</html>
