<?php
function convertForSQL($array){
    foreach(array_keys($array) as $key){
        if(is_string($array[$key])){
            $_POST[$key] = "'".$_POST[$key]."'";
        }
    }
}

function arrayToUl($array) {
    $html = '<ul>';
    foreach($array as $e) {
        $html .= '<li>'.$e.'</li>';
    }
    $html .= '</ul>';
    return $html;
}

function popup($content){
    $popup = '<div class="popup">';
    $popup .= $content.'</div>';
    return $popup;
}

function surround($tag, $attributes, $content) {
    return '<'.$tag.' '.$attributes.'>'.$content.'</'.$tag.'>';
}

function surround2($tag, $content) {
    return '<'.$tag.'>'.$content.'</'.$tag.'>';
}

?>
