<?php
    session_start();
?>
<!DOCTYPE HTML>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="../css/global.css" />
    <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
    <meta charset=utf-8>
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/script.js">


    </script>
    <title>Inscription</title>
</head>

<body>
    <?php
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        include('required.php');
    ?>
    <section id="inscription">
        <?php
        $errorMessages = array();
        $register = false;
        if(isset($_POST['submit'])){
            //On tente de faire l'inscription

            if(!isset($_POST['email']) || $_POST['email']==''){
                array_push($errorMessages,"Renseignez l'email");
            }
            if(!isset($_POST['pwd']) || $_POST['pwd']==''){
                array_push($errorMessages,"Renseignez le mot de passe.");
            }
            if(!isset($_POST['surname']) || $_POST['surname']==''){
                array_push($errorMessages,"Renseignez le nom de famille.");
            }
            if(!isset($_POST['name']) || $_POST['surname']==''){
                array_push($errorMessages,"Renseignez le prénom.");
            }
            if(!isset($_POST['phone']) || $_POST['phone']==''){
                array_push($errorMessages,"Renseignez le numéro de téléphone.");
            }
            if(!isset($_POST['gender']) || $_POST['gender']=='' ||
              $_POST['gender']=='null'){
                array_push($errorMessages,"Renseignez le genre.");
            }
            if(!isset($_POST['birthdate']) || $_POST['birthdate']=='' ||
              $_POST['birthdate']=='null'){
                array_push($errorMessages,"Renseignez votre date de naissance.");
            }

            //Si tout va bien jusque là, on se connecte à la BDD
            if(count($errorMessages) == 0){
                try{
                    if($dbh->query("SELECT COUNT(*) FROM USER WHERE EMAIL='".$_POST['email']."'")->fetch()[0]!=0){
                        // On vérifie la non-existence d'un utilisateur avec cet email.
                        array_push($errorMessages, 'Désolé, cet email est déjà utilisé.');
                    }
                    else{
                        $address = null;
                        if(isset($_POST['address'])){
                            $address = $_POST['address'];
                        }
                        $query = $dbh->prepare("INSERT INTO USER (EMAIL, SURNAME, NAME, PASSWORD, ADDRESS, PHONE, GENDER, BIRTHDATE, ROLE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, 'MEMBER')");
                        $query->execute(array($_POST['email'],
                                              $_POST['surname'],
                                              $_POST['name'],
                                              md5($_POST['pwd']),
                                              $address,
                                              $_POST['phone'],
                                              $_POST['gender'],
                                              $_POST['birthdate']));
                        echo '<h2>Inscription réussie !';
                        $register = true;

                        $_SESSION['sign_up'] = true;
                        $_POST['login'] = $_POST['email'];
                        $_POST['password'] = $_POST['pwd'];
                        $_POST['sign_in'] = true;
                    }
                } catch(PDOException $e) {
                    echo 'Unable to add the new user (Reason :'.$e->getMessage().')';
                    die();
                }
            }

        }
        //On affiche les erreurs s'il y en a.
        if(count($errorMessages) > 0){
            echo popup(arrayToUl($errorMessages));
        }
        ?>
            <form method="post" class="edit_profile" <?php if($register){ echo 'style="display: none"';}?>>
                <div>
                  <label for="email">Email</label>
                  <img src="../img/fieldneeded.svg" class="symbol">
                </div>
                <input id="email" name="email" type="email" required placeholder="your@mail.com" maxlength=254/>

                <div>
                  <label for="pwd">Mot de passe</label>
                  <img src="../img/fieldneeded.svg" class="symbol">
                </div>
                <input id="pwd" name="pwd" type="password" required/>

                <div>
                  <label for="surname">Nom de famille</label>
                  <img src="../img/fieldneeded.svg" class="symbol">
                </div>
                <input id="surname" name="surname" type="text" required maxlength=20/>

                <div>
                  <label for="name">Prénom</label>
                  <img src="../img/fieldneeded.svg" class="symbol">
                </div>
                <input id="name" name="name" type="text" maxlength=20/>

                <label for="address">Adresse</label>
                <input id="address" name="address" type="text" maxlength=200/>

                <div>
                  <label for="name">Date de naissance</label>
                  <img src="../img/fieldneeded.svg" class="symbol">
                </div>
                <input id="birthdate" name="birthdate" type="date" />

                <div>
                  <label for="phone">Téléphone</label>
                  <img src="../img/fieldneeded.svg" class="symbol">
                </div>
                <input id="phone" name="phone" type="text" pattern="[0-9]{10}" required placeholder="0600112233" />

                <div>
                    <label for="gender">Genre</label>
                    <img src="../img/fieldneeded.svg" class="symbol">
                </div>
                <select id="gender" name="gender" required>
                <option value="null">Veuillez sélectionner...</option>
                <option value="M">Homme</option>
                <option value="F">Femme</option>
            </select>

                <input type="submit" name="submit" value="S'inscrire" />

                <p>Les champs suivis de <img src="../img/fieldneeded.svg" class="symbol"> sont obligatoires pour procéder à l'inscription.</p>
            </form>
    </section>
<?php include('footer.php'); ?>
</body>
