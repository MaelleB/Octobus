<?php session_start();
ini_set('display_errors', 1);
error_reporting(E_ALL); ?>
<!DOCTYPE HTML>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="../css/global.css" />
    <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
    <meta charset=utf-8>
    <title>Trajets</title>
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/script.js">


    </script>
</head>

<body>
    <?php include('required.php'); ?>
    <section id="content">
      <?php
        if(isset($_POST["rideid"])){
          $ride = $_POST['rideid'];
          //On récupère les données concernant le conducteur du trajet
          //$query = $dbh->query("SELECT * FROM RIDE WHERE ID=".$ride)->fetchAll();
          $query = $dbh->prepare(
            "SELECT EMAIL, NAME, SURNAME, AGE(EMAIL) AS AGE
            FROM USER, TRAVELS_IN
            WHERE USER_ID=EMAIL
            AND TYPE='DRIVER'
            AND RIDE_ID=?");
          $query->execute(array($ride));
          $driver = $query->fetch();
          $html = surround2('p', surround('a', 'href="profile.php?userid='.urlencode($driver['EMAIL']).'"', $driver['NAME']." ".$driver['SURNAME']." (".$driver['AGE']." ans)"));
          $html = surround('article', 'class="driver"', $html);

          //On récupère le véhicule
          $query = $dbh->prepare(
            "SELECT CONCAT(MAKE, ' ',COLOR)
            FROM RIDE, VEHICLE
            WHERE RIDE.VEHICLE=VEHICLE.ID
            AND RIDE.ID=?");
          $query->execute(array($ride));
          $vehicle = $query->fetch()[0];
          $vehicle = surround2('p', 'Véhicule :<br>'.$vehicle);
          $html .= surround('article', 'class="driver"', $vehicle);

          //On récupère les passagers et on les affiche
          $query = $dbh->prepare(
            "SELECT EMAIL, NAME, SURNAME
            FROM TRAVELS_IN, USER
            WHERE USER_ID=EMAIL
            AND TRAVELS_IN.TYPE='PASSENGER'
            AND RIDE_ID=?");
          $query->execute(array($ride));
          $passengers = $query->fetchAll();

          $htmlPassengers = '';
          foreach($passengers as $row){
            $passenger = $row['NAME'].' '.$row['SURNAME'];
            if(isset($_SESSION['login']) && $driver['EMAIL'] == $_SESSION['login']) {
              //Si c'est le conducteur, on donne accès au profil
              $passenger = surround('a', 'href="profile.php?userid='.urlencode($row['EMAIL']).'"', $passenger);
            }
            $htmlPassengers .= surround2('li', $passenger);
          }
          $htmlPassengers = surround('article', 'class="passenger"', surround2('p', 'Passagers : ').surround2('ul', $htmlPassengers));
          $html .= $htmlPassengers;

          //On récupère les informations sur le trajet
          $query = $dbh->prepare(
            'SELECT NAME
            FROM CITY, RIDE
            WHERE POST_CODE=START_CITY
            AND ID = ?'
          );
          $query->execute(array($ride));
          $htmlInfos = surround2('strong', $query->fetch()[0]);
          $query = $dbh->prepare(
            'SELECT NAME
            FROM CITY, RIDE
            WHERE POST_CODE=ARRIVAL_CITY
            AND ID = ?'
          );
          $query->execute(array($ride));
          $htmlInfos .= " > ".surround2('strong', $query->fetch()[0]);

          $htmlInfos = surround2('p', $htmlInfos);

          //Récupération et affichage de l'heure de départ
          //Calcul de la durée estimée du trajet
          $query = $dbh->prepare(
            'SELECT HOUR(START_DATE) AS HOUR,
                    M_FORMAT(MINUTE(START_DATE)) AS MINUTE,
                    DAY(START_DATE) AS DAY,
                    MONTH(START_DATE) AS MONTH,
                    HOUR(TIMEDIFF(ARRIVAL_DATE, START_DATE)) AS DURATION_HOUR,
                    M_FORMAT(MINUTE(TIMEDIFF(ARRIVAL_DATE, START_DATE))) AS DURATION_MINUTE,
                    PRICE,
                    SEAT_NB
            FROM RIDE
            WHERE ID = ?');
          $query->execute(array($ride));
          $result = $query->fetch();
          $htmlInfos .= surround2('p', surround2('strong', 'Départ à : ').$result['HOUR']."h".$result['MINUTE'].' (le '.$result['DAY'].'/'.$result['MONTH'].')');

          $htmlInfos .= surround2('p', surround('strong', '', 'Durée estimée : ')
                        .$result['DURATION_HOUR'].'h'
                        .$result['DURATION_MINUTE']);
          $htmlInfos .= surround2('p', surround('strong', '', 'Prix du trajet : ').$result['PRICE'].'€');
          $htmlInfos .= surround2('p', surround2('strong', 'Nombre de places disponibles : ').$result['SEAT_NB']);
          $htmlInfos = surround('article', 'class="infos"', $htmlInfos);

          $html .= $htmlInfos;

          if(isset($_SESSION['login'])) {
            if($dbh->query("SELECT COUNT(*)
                            FROM TRAVELS_IN
                            WHERE USER_ID='".$_SESSION['login']."'
                            AND RIDE_ID=".$ride)->fetch()[0] == 0){
              //Si l'utilisateur est inscrit au trajet
              $html .='<article class="form">
                  <form method="post">
                      <div class="labelfield">
                          <input type="hidden" name="rideid" value="'.$ride.'"/>
                          <label for="place"> Nombre de places </label>
                          <input id="place" name="place" type="number" min=1 required/>
                      </div>
                      <input type="submit" name="book" value="Réserver" />
                  </form>
              </article>';
            }
            else{
              $query = $dbh->prepare(
                'SELECT ARRIVAL_DATE > NOW()
                FROM RIDE
                WHERE ID = ?'
              );
              $query->execute(array($ride));
              if($query->fetch()[0]) { //Si le trajet n'est pas passée
                $html .='<article class="form">
                    <form method="post">
                        <input type="hidden" name="rideid" value="'.$ride.'"/>
                        <input type="submit" name="cancel" value="Annuler ce trajet" />
                    </form>
                </article>';
              }
              else {
                $html .=
                '<article class="form">
                    <form method="post" action="opinion.php">
                        <input type="hidden" name="ride" value="'.$ride.'"/>
                        <input type="submit" name="opinion" value="Donner un avis" />
                    </form>
                </article>';
              }
            }
          }

          $html = surround('section', 'class="ride"', $html);
          echo $html;
        }

        if(isset($_POST["place"])){
          if($_POST["place"] <= $dbh->query("SELECT SEAT_NB FROM RIDE WHERE ID=".$ride)->fetch()[0]){
            $query = $dbh->prepare("INSERT INTO TRAVELS_IN VALUES (?, ?, 'PASSENGER', ?)");
            $query->execute(array($_SESSION['login'], $ride, $_POST['place']));
            echo "<h2>Réservation bien prise en compte</h2>";
          }
        }

        if(isset($_POST["cancel"])){
          $query = $dbh->prepare("CALL CANCELLATION_TRAVELS_IN(?, ?)");
          $query->execute(array($ride, $_SESSION['login']));

          echo "<h2>Annulation bien prise en compte. Les personnes concernées ont été averties.</h2>";
        }

     ?>

        <!--
        <section class="ride">
            <article class="rider">
                <p>Zetarov (20 ans) - 10<img class="symbol" src="../img/star.png" /></p>
            </article>
            <article class="passenger">
                <p>Passagers :</p>
                <ul>
                    <li>Llylaïa</li>
                    <li>Evoluxe</li>
                </ul>
            </article>
            <article class="info">
                <p>
                    <strong><?='Montpellier'?></strong> >
                    <strong><?='Fos-sur-Mer'?></strong>
                </p>
                <p>
                    <strong>Durée estimée :</strong> 1h30min
                </p>
                <p>
                    <strong>Prix du trajet :</strong> 7€
                </p>
                <p>
                    <strong>Détails :</strong> Un exemple de trajet (qui serait dans la BDD).
                </p>
            </article>

        </section> -->
    </section>
    <?php include('footer.php'); ?>
</body>

</html>
