<?php session_start(); ?>
<!DOCTYPE HTML>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="../css/global.css" />
    <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
    <meta charset=utf-8>
    <title>Trajets</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../js/script.js"></script>
    <title>Donner un avis</title>
</head>

<body>
    <?php include('required.php'); ?>
    <section id="content">
      <section class="center">
      <?php
        if (isset($_POST['give']) && isset($_POST['ride'])) {
          $correct = false;
          if(isset($_POST['comment']) && isset($_POST['stars'])) {
            $correct = true;
            $cpt_comment = 0;
            $cpt_stars = 0;
            foreach ($_POST['comment'] as $comment) {
              if($comment == "") {
                $correct = false;
              }
              else {
                $cpt_comment++;
              }
            }
            foreach ($_POST['stars'] as $stars) {
              if($stars == "") {
                $correct = false;
              }
              $cpt_stars++;
            }
            if($correct) {
              $correct = ($cpt_comment == $cpt_stars);
            }
          }
          if(!$correct) {
            echo popup(surround2('p', 'Veuillez renseigner tous les champs.'));

          }
          else { //On fait l'insertion des avis.
              for($i = 0; $i < count($_POST['stars']); ++$i) {
                $query = $dbh->prepare(
                  'CALL GIVE_OPINION_ON(?, ?, ?, ?, ?)'
                );
                $query->execute(array($_POST['ride'],
                  $_SESSION['login'],
                  $_POST['user_concerned'][$i],
                  $_POST['comment'][$i],
                  $_POST['stars'][$i]));
              }
              echo surround2('h1', 'Votre avis a été posté !')
          }
        }
        else if(isset($_POST['opinion']) && isset($_POST['ride']) && $_SESSION['login']) {
          $query = $dbh->prepare(
            'SELECT TYPE
            FROM TRAVELS_IN
            WHERE USER_ID = ?
            AND RIDE_ID = ?');
          $query->execute(array($_SESSION['login'], $_POST['ride']));
          $type = $query->fetch()[0];
          if($type == 'PASSENGER') {
            //On récupère le conducteur
            $query = $dbh->prepare(
              "SELECT EMAIL, NAME, SURNAME
              FROM USER, TRAVELS_IN
              WHERE EMAIL = USER_ID
              AND TRAVELS_IN.TYPE = 'DRIVER'
              AND RIDE_ID = ?"
            );
            $query->execute(array($_POST['ride']));
            $driver = $query->fetch();
            ?>
            <form method="post" class="edit_profile stylish-container" id="opinion_form">
              <h2>Donner votre avis sur <?= $driver['NAME'] ?> <?= $driver['SURNAME'] ?></h2>

              <input type="hidden" name="user_concerned[]" value="<?= $driver['EMAIL']?>" />
              <textarea maxlength=300 name="comment[]" minlength=30 required placeholder="Votre avis..."></textarea>
              <div class="space-around">
                <div>
                <input required type="number[]" min=1 max=10 name="stars[]" id="stars"/>
                <img src="../img/star.png" class="symbol"/>
              </div>
              <input type="hidden" name="ride" value="<?= $_POST['ride'] ?>" />
              <input type="submit" name="give" value="Envoyer"/>
              </div>

            </form>
            <div/>
            <?php
          }
          else {
            //On récupère les passagers
            $query = $dbh->prepare(
              "SELECT EMAIL, NAME, SURNAME
              FROM USER, TRAVELS_IN
              WHERE EMAIL = USER_ID
              AND TRAVELS_IN.TYPE = 'PASSENGER'
              AND RIDE_ID = ?"
            );
            $query->execute(array($_POST['ride']));
            $passengers = $query->fetchAll();
            ?>
            <form method="post" class="edit_profile stylish-container" id="opinion_form">
            <?php
            foreach($passengers as $passenger) {
            ?>
              <div>
                <h2>Donner votre avis sur <?= $passenger['NAME']?> <?= $passenger['SURNAME']?></h2>

              </div>
              <input type="hidden" name="user_concerned[]" value="<?= $passenger['EMAIL']?>" />
              <textarea maxlength=300 name="comment[]" minlength=30 required placeholder="Votre avis..."></textarea>
              <div class="space-around">
                <div>
                <input type="number" min=1 max=10 name="stars[]" id="stars" required/>
                <img src="../img/star.png" class="symbol"/>
              </div>

              </div>
              <?php
              }
            ?>
              <input type="hidden" name="ride" value="<?= $_POST['ride'] ?>" />
              <input type="submit" name="give" value="Envoyer"/>
            </form>
            <?php
          }
        }

        ?>
      </section>
    </section>
    <?php include('footer.php'); ?>
</body>

</html>
