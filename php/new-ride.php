<?php session_start();
ini_set('display_errors', 1);
error_reporting(E_ALL);?>
<!DOCTYPE HTML>
<html>

<head>
  <link rel="stylesheet" type="text/css" href="../css/global.css" />
  <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
  <meta charset=utf-8>
  <title>Trajets</title>
  <script src="../js/jquery-3.2.1.min.js"></script>
  <script src="../js/script.js"></script>
  <title>Proposer un trajet</title>
</head>

<body>
  <?php include('required.php'); ?>
  <section id="content">
    <section id="inscription">
      <?php if(isset($_SESSION['login'])) {
        $hour = '';$min = '';$price = '';$start = '';$arrival = '';
        if(isset($_POST['prefill'])) {
          if(isset($_POST['start_city']) && isset($_POST['arrival_city']) && $_POST['start_city'] != '' && $_POST['arrival_city'] != '') {
            $query = $dbh->prepare(
              'SELECT HOUR(DURATION), MINUTE(DURATION), MAX_COST/2
              FROM MODELRIDE
              WHERE START = ?
              AND ARRIVAL = ?'
            );
            $query->execute(array($_POST['start_city'], $_POST['arrival_city']));
            $result = $query->fetch();
            $hour = $result[0];
            $min = $result[1];
            $price = $result[2];
            $start = $_POST['start_city'];
            $arrival = $_POST['arrival_city'];
          }
          else {
            echo popup(surround2('p', 'Pour le préremplissage, veuillez remplir les codes postaux des villes de départ et d\'arrivée'));
          }
        }?>
      <form class="edit_profile center" method="post">
        <div class="rowcenter">
          <div class="field">
            <div class="label">
              <label for="start_city">Ville de départ (code postal)</label>
              <img src="../img/fieldneeded.svg" class="symbol">
            </div>
            <input type="text" name="start_city" id="start_city"  placeholder="34000" value="<?= $start ?>"/>
          </div>
          <div class="field">

            <div class="label">
              <label for="arrival_city">Ville d'arrivée (code postal)</label>
              <img src="../img/fieldneeded.svg" class="symbol">
            </div>
            <input type="text" name="arrival_city" id="arrival_city"  placeholder="34000" value="<?= $arrival ?>"/>
          </div>
        </div>

        <input type="submit" style="align-self:unset;" name="prefill" value="Essayer de préremplir" />

        <div class="rowcenter">
          <div class="field">
            <div class="label">
              <label for="start_address">Point de rendez-vous (départ)</label>
            </div>
            <input type="text" name="start_address" id="start_address" placeholder="Gare" />
          </div>
          <div class="field">
            <div class="label">
              <label for="arrival_address">Point de rendez-vous (arrivée)</label>
            </div>
            <input type="text" name="arrival_address" id="arrival_address" placeholder="Place de la Comédie" />
          </div>
        </div>

        <div>
          <label for="start_date">Date du trajet</label>
          <img src="../img/fieldneeded.svg" class="symbol">
        </div>
        <input type="date" name="start_date" id="start_date" />

        <div>
          <label for="hour">Heure de départ</label>
          <img src="../img/fieldneeded.svg" class="symbol">
        </div>
        <div class="rowcenter">
        <input type="number" name="hour" id="hour" min=0 max=23  placeholder="18" />h
        <input type="number" name="min" id="min" min=0 max=59  placeholder="15" />
      </div>

        <div>
          <label for="hour2">Temps de trajet</label>
          <img src="../img/fieldneeded.svg" class="symbol">
        </div>
        <div class="rowcenter">
        <input type="number" name="duration_hour" id="" min=0 max=23  placeholder="1" value="<?= $hour ?>"/>h
        <input type="number" name="duration_min" id="" min=0 max=59  placeholder="20" value="<?= $min ?>"/>
      </div>

      <div>
        <label for="vehicle">Véhicule</label>
        <img src="../img/fieldneeded.svg" class="symbol">
      </div>
        <select id="vehicle" name="vehicle">
            <?php
              $query = $dbh->prepare(
                'SELECT ID, MAKE, COLOR
                FROM VEHICLE
                WHERE USER_ID = ?'
              );
              $query->execute(array($_SESSION['login']));
              $result = $query->fetchAll();
              foreach($result as $row) {
                echo surround('option', 'value="'.$row[0].'"', $row[1].' '.$row[2]);
              }
            ?>
            </select>

            <div>
              <label for="seat_nb">Nombre de places</label>
            </div>
            <input type="number" name="seat_nb" min=1 max=10 id="arrival_city" value="1"/>

            <div>
              <label for="price">Prix du trajet</label>
              <img src="../img/fieldneeded.svg" class="symbol">
            </div>
            <div class="rowcenter">
            <input type="number" name="price" id="price" step=0.05 min=0.00 max=999.00 value="<?= $price ?>" />€
          </div>


        </br>
        <input type="submit" name="new-ride" value="Valider" style="align-self: unset;"/>

        <p>Les champs suivis de <img src="../img/fieldneeded.svg" class="symbol"> sont obligatoires pour proposer un trajet.</p>
      </form>
      <?php }
      else{
          ?>
      <h2>Pour proposer un trajet, vous devez être connecté !</h2>
      <a href="rides.php">Cliquez ici pour retourner aux trajets.</a>
      <?php
      }
          if(isset($_POST['new-ride'])){

            //Vérification de l'entrée des valeurs facultatives
            if(isset($_POST['start_address'])){
              $startaddress = $_POST['start_address'];
            }
            else{
              $startaddress = NULL;
            }

            if(isset($_POST['arrival_address'])){
              $arrivaladdress = $_POST['arrival_address'];
            }
            else{
              $arrivaladdress = NULL;
            }

            if(isset($_POST['seat_nb'])){
              $seatnb = $_POST['seat_nb'];
            }
            else{
              $seatnb = 1;
            }

            //Vérification que tout est rentré dans les valeurs obligatoires
            if(isset($_POST['start_date']) && $_POST['start_date'] != "" &&
              isset($_POST['duration_hour']) && $_POST['duration_hour'] != "" &&
              isset($_POST['duration_min']) && $_POST['duration_hour'] != "" &&
              isset($_POST['price']) && $_POST['price'] != "" &&
              isset($_POST['start_city']) && $_POST['start_city'] != "" &&
              isset($_POST['arrival_city']) && $_POST['arrival_city'] != "" &&
              isset($_POST['vehicle']) && $_POST['vehicle'] != "") {

                $date1 = $_POST['start_date']." ".$_POST['hour'].":".$_POST['min'].":00";
                $query = $dbh->prepare(
                  'SELECT TIMESTAMPADD(HOUR, ?, TIMESTAMPADD(MINUTE, ?, ?))'
                );
                $query->execute(array($_POST['duration_hour'], $_POST['duration_min'], $date1));
                $date2 = $query->fetch()[0];

                //On vérifie que le prix ne dépasse pas celui du modèle de trajet s'il en existe un
                $query = $dbh->prepare(
                  'SELECT ? < MAX_COST, TRUNCATE(MAX_COST,2)
                  FROM MODELRIDE
                  WHERE START=?
                  AND ARRIVAL=?'
                );
                $query->execute(array($_POST['price'], $_POST['start_city'], $_POST['arrival_city']));
                $result = $query->fetch();
                if($result[0] != NULL && !$result[0]){ //Au dessus du prix
                  echo popup(surround2('p', 'Le prix ne devrait pas dépasser '.$result[1]. '€, veuillez réessayer avec un prix plus faible !'));
                }
                else {
                  //On crée une entrée dans la table de trajet et on ajoute le conducteur dans TRAVELS_IN
                  $query = $dbh->prepare("INSERT INTO RIDE (ID, START_DATE, ARRIVAL_DATE, PRICE, SEAT_NB, START_CITY, ARRIVAL_CITY, START_ADDRESS, ARRIVAL_ADDRESS, VEHICLE) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                  $query->execute(array($date1, $date2, $_POST['price'], $seatnb, $_POST['start_city'], $_POST['arrival_city'], $startaddress, $arrivaladdress, $_POST['vehicle']));

                  $query2 = $dbh->prepare("INSERT INTO TRAVELS_IN (RIDE_ID, USER_ID, TYPE, SEAT_NUMBER) VALUES ((SELECT MAX(ID) FROM RIDE), ?, 'DRIVER', 1)");
                  $query2->execute(array($_SESSION['login']));

                  echo popup("<h2>Le trajet a bien été ajouté.</h2>");
                }
            }
            else {
              echo popup(surround2('p', 'Veuillez remplir tous les champs obligatoires !'));
            }


          }
        ?>
    </section>
  </section>
  <?php include('footer.php'); ?>
</body>
