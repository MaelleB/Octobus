/*
21507117, BEURET Maëlle
21607805, LESINSKI Julien
*/

INSERT INTO `CITY` (`POST_CODE`, `NAME`) VALUES
('11100', 'Narbonne'),
('13000', 'Marseille'),
('13200', 'Arles'),
('13270', 'Fos'),
('13990', 'Fontvieille'),
('30000', 'Nimes'),
('30100', 'Alès'),
('34000', 'Montpellier'),
('34500', 'Béziers'),
('83000', 'Toulon');

INSERT INTO `MODELRIDE` (`START`, `ARRIVAL`, `DURATION`, `DISTANCE`, `FREQUENCY`, `MAX_COST`) VALUES
('13000', '13200', NULL, NULL, 0, NULL),
('13200', '13000', NULL, NULL, 0, NULL),
('13270', '30000', '00:57:00', 71, 0, '10.100'),
('13270', '34000', '01:30:00', 120, 0, '12.000'),
('30100', '34000', NULL, NULL, 0, NULL),
('34000', '13270', '01:30:00', 120, 0, '12.000'),
('34000', '30100', NULL, NULL, 0, NULL),
('34000', '34500', NULL, NULL, 0, NULL),
('34500', '34000', NULL, NULL, 0, NULL);

INSERT INTO `USER` (`EMAIL`, `SURNAME`, `PASSWORD`, `BIRTHDATE`, `NAME`, `ADDRESS`, `PHONE`, `ROLE`, `BAN_ID`, `GENDER`) VALUES
('amandine@mail.com', 'Paillard', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', '1997-02-02', 'Amandine', '', '0601020304', 'MEMBER', NULL, 'F'),
('christelle@mail.com', 'Lallemand', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', '1972-11-27', 'Christelle', '', '0633445566', 'MEMBER', NULL, 'F'),
('coroad@mail.com', 'CoRoad', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', '1980-01-05', 'Jean', '', '0678910111', 'ADMIN', NULL, 'M'),
('cynthia@mail.com', 'Cheillan', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', '1999-05-21', 'Cynthia', '', '0644556677', 'MEMBER', NULL, 'F'),
('felix@mail.com', 'Costa', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', '1997-07-25', 'Félix', '', '0602030405', 'MEMBER', NULL, 'M'),
('julie@mail.com', 'Cailler', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', '1997-07-04', 'Julie', '', '0666778899', 'MEMBER', NULL, 'F'),
('julien@mail.com', 'Lesinski', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', '1997-06-09', 'Julien', '', '0611223344', 'MEMBER', NULL, 'M'),
('maelle@mail.com', 'Beuret', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', '1997-09-05', 'Maëlle', '', '0600112233', 'MEMBER', NULL, 'F'),
('thomas@mail.com', 'Georges', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', '1997-01-01', 'Thomas', '', '0622334455', 'MEMBER', NULL, 'M'),
('vincent@mail.com', 'Collela', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', '1994-10-04', 'Vincent', '', '0655667788', 'MEMBER', NULL, 'M');

INSERT INTO `VEHICLE` (`ID`, `USER_ID`, `MAKE`, `COLOR`) VALUES
(1, 'thomas@mail.com', 'Fiat Panda', 'Verte'),
(2, 'maelle@mail.com', 'Fiat 500', 'Rouge'),
(3, 'julien@mail.com', 'Peugeot 206', 'Grise'),
(4, 'julie@mail.com', 'Twingo', 'Blanche'),
(5, 'amandine@mail.com', '4x4', 'Gris');

INSERT INTO `NEGATIVE_USER` (`USER_ID`) VALUES
('felix@mail.com'),
('vincent@mail.com');

INSERT INTO `RIDE` (`ID`, `START_DATE`, `ARRIVAL_DATE`, `PRICE`, `SEAT_NB`, `START_CITY`, `ARRIVAL_CITY`, `START_ADDRESS`, `ARRIVAL_ADDRESS`, `VEHICLE`) VALUES
(1, '2017-12-21 17:15:00', '2017-12-21 18:00:00', '6.00', 2, '13200', '13000', '', '', 1),
(2, '2017-12-22 19:00:00', '2017-12-22 19:50:00', '6.00', 2, '13000', '13200', '', '', 1),
(3, '2017-12-21 09:50:00', '2017-12-21 10:30:00', '5.00', 2, '34000', '30100', '', '', 2),
(4, '2017-12-24 14:30:00', '2017-12-24 15:15:00', '4.00', 3, '30100', '34000', '', '', 2),
(5, '2017-12-21 13:30:00', '2017-12-21 15:00:00', '8.00', 3, '34000', '13270', '', '', 3),
(6, '2017-12-24 19:15:00', '2017-12-24 20:45:00', '7.50', 3, '13270', '34000', '', '', 3),
(7, '2017-12-23 09:00:00', '2017-12-23 09:45:00', '5.00', 4, '13270', '30000', '', '', 3),
(8, '2017-12-22 19:00:00', '2017-12-22 19:55:00', '5.00', 4, '34000', '34500', '', '', 4),
(9, '2017-12-24 18:30:00', '2017-12-24 19:25:00', '6.00', 4, '34500', '34000', '', '', 4),
(10, '2017-12-25 14:10:00', '2017-12-25 15:05:00', '4.00', 4, '34000', '34500', '', '', 4),
(11, '2017-12-28 14:10:00', '2017-12-28 15:05:00', '4.00', 4, '34000', '34500', '', '', 4),
(12, '2017-12-22 14:00:00', '2017-12-22 15:20:00', '7.50', 2, '13270', '34000', '', '', 5),
(13, '2017-12-29 14:00:00', '2017-12-29 15:20:00', '7.50', 2, '13270', '34000', '', '', 5),
(14, '2017-12-25 14:00:00', '2017-12-25 15:40:00', '8.50', 2, '34000', '13270', '', '', 5);

INSERT INTO `OPINION` (`ID`, `STARS`, `COMMENT`, `AUTHOR`, `USER_ID`, `USER_TYPE`, `RIDE_ID`) VALUES
(1, '7', 'Conductrice agréable ! Je recommande !', 'cynthia@mail.com', 'julie@mail.com', 'PASSENGER', NULL);

INSERT INTO `TRAVELS_IN` (`USER_ID`, `RIDE_ID`, `TYPE`, `SEAT_NUMBER`) VALUES
('amandine@mail.com', 12, 'DRIVER', 1),
('amandine@mail.com', 13, 'DRIVER', 1),
('amandine@mail.com', 14, 'DRIVER', 1),
('julie@mail.com', 8, 'DRIVER', 1),
('julie@mail.com', 9, 'DRIVER', 1),
('julie@mail.com', 10, 'DRIVER', 1),
('julie@mail.com', 11, 'DRIVER', 1),
('julien@mail.com', 5, 'DRIVER', 1),
('julien@mail.com', 6, 'DRIVER', 1),
('julien@mail.com', 7, 'DRIVER', 1),
('maelle@mail.com', 3, 'DRIVER', 1),
('maelle@mail.com', 4, 'DRIVER', 1),
('thomas@mail.com', 1, 'DRIVER', 1),
('thomas@mail.com', 2, 'DRIVER', 1),
('amandine@mail.com', 8, 'PASSENGER', 2);
